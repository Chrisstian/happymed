Telepítési útmutató

A program symfony keretrendszert használ.
A program php 8.2-es verziót igényel.
A symfony telepítéséhez composer-re lesz szükség.
Composer letöltése: https://getcomposer.org/download/

Amennyiben a megfelelő verziójú php és composer rendelkezésre állnak, lehet telepíteni a symfony-t.
"composer install" paranccsal települ a symfony, a composer.json alapján.

Telepítés után, be kell állítani, a környezeti változókat.
A környezeti változók, az .env file-ban találhatók. Erről a file-ról, kell készíteni, egy másolatot, .env.local néven.
Az új file-ban (.env.local), a következő értékeket kell beállítani:
    - a nem kikommentezett DATABASE_URL változót, ami mysql alapú (minta: mysql://userName:Password@127.0.0.1:3306/databaseName)

Miután az adatbázis kapcsolat beállításra került, kell készíteni, egy php kiterjesztésű környezeti file-t is. Ezt a composer megcsinálja helyettünk.
"composer dump-env" paranccsal elkészül, az .env.local.php file.
Ezzel meg is vagyunk, a környezeti változók beállításaival.

Doctrine ORM használata miatt, létezik egy adatbázis migrációs file. (migrations mappa)
Ez egy user táblát tartalmaz, amit üresen létre kell hozni, a következő képpen.
Ezt a két parancsot kell lefuttani:
"php bin/console make:migration" - ez megnézi, hogy milyen különbséged vannak az entitások és az adatbázis között
"php bin/console doctrine:migrations:migrate" - a létrehozott migrációs file-okat lefuttatja


Program futtatása

Összesen 3 végpontot hoztam létre, amelyek a következők: /login, /users és /registration
Mind a 3 végpont elérhető GET-es http protokollal. Természetesen a bejelentkezés és a regisztráció POST-os protokollal is elérhető.
Természetesen a /users végpont nem érhető el bejelentkezés nélkül.
A végpontokat natívan, így lehet elérni: http://ipcím/projectMappaNeve/public/index.php/vegpont

Remélem gond és hiba nélkül sikerül működésre bírni a rendszert.
Amennyiben probléma merül fel, keressetek bizalommal.