<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LoginForm
 */
class LoginForm extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('_username', TextType::class, ['label' => 'Email cím', 'required' => false, 'mapped' => false, 'attr' => ['class' => 'form-control', 'placeholder' => 'E-mail cím']])
            ->add('_password', PasswordType::class, ['label' => 'Jelszó', 'required' => false, 'mapped' => false, 'attr' => ['class' => 'form-control', 'placeholder' => 'Jelszó']])
            ->add('submit', SubmitType::class, ['label' => 'Belépés', 'attr' => ['class' => 'btn btn-primary']]);
    }

    /**
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver) {

        $resolver->setDefaults(['data_class' => User::class]);
    }
}