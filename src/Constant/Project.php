<?php

namespace App\Constant;

/**
 * Class Project
 */
class Project {

    const EMPTY_FIELD_ERROR_MSG = 'Kérjük töltse ki az összes mezőt';
    const NOT_VALID_LOGIN_ERROR_MSG = 'Rossz e-mail cím vagy jelszó';
    const NOT_SAME_PASSWORD_ERROR_MSG = 'A két jelszó eltérő';
    const REGISTRATION_SUCCESSFUL_MSG = 'Sikeres regisztráció';
    const DUPLICATE_REGISTRATED_ERROR_MSG = 'Ez az e-mail cím már reigsztrálásra került';
    const DUPLICATE_ENTRY_ERROR_MSG = 'Duplicate entry';

    const IS_ACTIVE = 1;
}