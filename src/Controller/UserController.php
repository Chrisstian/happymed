<?php

namespace App\Controller;

use App\Constant\Project;
use App\Entity\User;
use App\Form\RegistrationForm;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 */
class UserController extends AbstractController {

    /**
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    #[Route('/users', name: 'users', methods: ['GET'])]
    public function user(EntityManagerInterface $entityManager): Response {

        $users = array();

        try {
            $users = $entityManager->getRepository(User::class)->findAll();
        } catch (Exception $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->render('user.html.twig', ['form' => $users]);
    }

    /**
     * Regisztráció
     *
     * @param Request $request
     * @param UserPasswordHasherInterface $passwordHasher
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    #[Route('/registration', name: 'registration', methods: ['POST', 'GET'])]
    public function registration(Request                     $request,
                                 UserPasswordHasherInterface $passwordHasher,
                                 EntityManagerInterface      $entityManager): Response {

        $user = new User();
        $form = $this->createForm(RegistrationForm::class, $user);

        try {
            if($request->isMethod('POST')) {
                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {
                    $formData = $form->getData();

                    $password = $request->request->all('registration_form')['password']['first'];
                    $passwordRepeat = $request->request->all('registration_form')['password']['second'];

                    if(empty($formData->getName()) || empty($formData->getEmail())) {
                        throw new Exception(Project::EMPTY_FIELD_ERROR_MSG);
                    } else if($password === '' || $passwordRepeat === '') {
                        throw new Exception(Project::EMPTY_FIELD_ERROR_MSG);
                    } else {
                        date_default_timezone_set('Europe/Budapest');
                        $hashedPassword = $passwordHasher->hashPassword($user, $formData->getPassword());
                        $user->setName($formData->getName());
                        $user->setEmail($formData->getEmail());
                        $user->setRoles($formData->getRoles());
                        $user->setPassword($hashedPassword);
                        $user->setIsActive(Project::IS_ACTIVE);
                        $user->setCreatedAt(new DateTimeImmutable(date('Y-m-d H:i:s')));
                        $entityManager->persist($user);
                        $entityManager->flush();
                        $this->addFlash('success', Project::REGISTRATION_SUCCESSFUL_MSG);
                    }
                }
                else {
                    throw new Exception(Project::NOT_SAME_PASSWORD_ERROR_MSG);
                }
            }
        } catch (Exception $e) {
            if(str_contains($e->getMessage(), Project::DUPLICATE_ENTRY_ERROR_MSG)) {
                $this->addFlash('error', Project::DUPLICATE_REGISTRATED_ERROR_MSG);
            } else {
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('registration.html.twig', ['form' => $form]);
    }
}